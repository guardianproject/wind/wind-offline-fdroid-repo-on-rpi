#!/bin/bash

# Run by udev when a USB drive is inserted
# usage: /usr/bin/on-usb-drive-mounted.sh /media/%k

# If the drive inserted contains a directory named "butter",
# links the conf-available/50-usb-butter.conf file to 
# conf-enabled/50-usb-butter.conf and restarts lighttpd.
device="$1"

# The device might not be mounted yet, so wait for it.
usb_mount_path=""
for ((i=0; i<10; i++)); do
    usb_mount_path=$(findmnt -n -o TARGET --source "$device")
    if [ -n "$usb_mount_path" ]; then
        break
    fi
    sleep 1
done

# findmnt will briefly return 1, so don't set e until we're done with it.
set -e

if [ -z "$usb_mount_path" ]; then
    echo "Device $device is not mounted"
    exit 1
else
    echo "Device $device mounted to: $usb_mount_path"
fi

butter_dir="$usb_mount_path"
conf_available="/etc/lighttpd/conf-available/50-usb-butter.conf"
conf_enabled="/etc/lighttpd/conf-enabled/50-usb-butter.conf"
served_dir="/media/usb-butter"

# make directory butter_dir world readable
sudo chmod -R a+rx "$butter_dir"
sudo chmod -R a+rx "/media/root/"

if [ -d "$butter_dir" ]; then
    # Delete served_dir if it exists
    if [ -L "$served_dir" ]; then
        sudo rm "$served_dir"
    fi
    echo "Linking $butter_dir to $served_dir"
    ln -sf "$butter_dir" "$served_dir"
    sudo chown -R www-data:www-data $served_dir
    echo "Linking $conf_available to $conf_enabled"
    ln -sf "$conf_available" "$conf_enabled"
    systemctl restart lighttpd
else
    echo "No butter directory $butter_dir found on $device"
    exit 1
fi
