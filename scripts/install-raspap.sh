set -e

# So that the admin interface loads in the user's browser-specified language
apt-get install locales-all -y
apt-get install dhcpcd -y

# Install RaspAP, accepting defaults and not installing adblock since this is
# an offline appliance.
curl -sL https://install.raspap.com | sudo bash -s -- --yes --path /var/www/html/admin --wireguard 0 --openvpn 0 --adblock 0 --branch 3.2.4

# if any directories starting with /var/www/html. exist, delete them
find /var/www/html.* -maxdepth 0 -type d -exec rm -r {} \; || :

# Configure the RaspAP network
cp $butter_src_dir/configs/butterbox-hostapd.conf /etc/hostapd/hostapd.conf
sed -i "s/REPLACEME/$butter_name/g" /etc/hostapd/hostapd.conf

# User our own hostapd.service in lieu of RaspAP's own.
# For reasons I don't understand, theirs works in an ordinary install, but not our provisioning
cp $butter_src_dir/configs/raspapd.service /lib/systemd/system/raspapd.service

# Configure the dnsmasq to respond at butterbox.local instead of 10.3.141.1
cp $butter_src_dir/configs/butterbox-dnsmasq.conf /etc/dnsmasq.d/butterbox-dnsmasq.conf

sed -i "s/REPLACEME/$butter_name/g" /etc/dnsmasq.d/butterbox-dnsmasq.conf