# Butter Box User Guide

## What is a Butter Box?

When you can't access the Internet, a Butter Box makes life offline a bit smoother.  

A Butter Box is a device you can access using the wifi connection on your
smartphone or computer.  When you're connected, you can install Android apps
like Open Street Maps, a Survival Manual, or even games to occupy your
children (or yourself!).  You can also use the Butter Box to chat publically
or privately.

## Setting up a Butter Box

Simply plug it in using the included power adapter or a suitable micro USB
power source.  In a few seconds, you'll see it's broadcasting the wifi
network "butterbox".

![Plug the butter box into an
outlet](images/plugged-in-to-outlet.jpg){width=40%}

![Plug the butter box into a solar power
supply](images/plugged-in-to-solar.jpg){width=40%}

The Butter Box works without an Internet connection and comes pre-loaded
with everything it needs.  The first time you plug it in, it may take
several minutes to start up.  After that, it will start faster.

## Connecting to the Butter Box

Using your phone or computer, connect to the "butterbox" wifi network.  To
make sure your device seeks information from the Butter Box instead of the
broader internet, turn off your phone's cellular connection.  You can also
join the network using this QR Code:

![Connect to the butterbox wifi
network](images/en/qr/ssid-butterbox.png){width=40%}

Next, visit `http://butterbox.lan`.  You can visit that address by scanning
this QR code:

![Visit the Butter Box's
homepage](images/en/qr/butterbox.lan.png){width=40%}

### Installing Android Apps

Once you've connected and loaded the Butter Box homepage, click "Download
Android App" then click "Download Android App" again from the modal.  This
will download a file called "butter.apk" to your device.

![Download butter.](images/en/download.jpg){width=40%}

Open "butter.apk".  You may need to allow installation from unknown
sources.  This will install the Butter App, which you can now open.

![The butter app on the homepage](images/en/app.jpg){width=40%}

After the Butter App loads the repository, you'll see a list of the
available apps.  Click any app to read more and then "Install" to install
the app to your device.

![The butter app store listing
apps](images/en/butter-app-store.jpg){width=40%}

### Chatting with other Butter Box users

Once you've connected and loaded the Butter Box homepage, you can choose to
join a public chat room or start a private chat using the buttons on the
http://butterbox.lan homepage.

Like a bulletin board, you can see all the messages others have left on the
public chat and any messages you leave on the public chat will be available
to others who connect later.

If you want to have a private conversation, you can create your own chatroom.  When you create a private room, only those you invite can enter and all the messages you send will be encrypted so only you and your invitees can see them.  Not even the Butter Box's creators or administrators can see your private messages.  

To invite others to the chatroom you created, click the icon at the top
left, then click the small QR code to enlarge the QR code so your friends
can scan it.  Your friends join the room by connecting to the Butter Box
wifi then scanning the QR code.

## Recherche de pannes

If you've connected to the wifi, but can't get the homepage to load, make
sure your cellular data is off.

If the Butter Box isn't working right, the easiest step is to restart it by
unplugging it, waiting 10 seconds, and plugging it back in.

If restarting the Butter Box doesn't fix it, ask your administrator for
help.
