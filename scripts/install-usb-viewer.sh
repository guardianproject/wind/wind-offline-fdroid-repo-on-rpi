set -e

# Using udev to mount newly attached usb drives doesn't work.
# https://unix.stackexchange.com/a/507150/223286
# So, create a systemd service udev can trigger to mount the drive.
cp $butter_src_dir/configs/udisks2-mount@.service /etc/systemd/system/
cp $butter_src_dir/configs/serve-usb@.service /etc/systemd/system/

# Use the CSS from the butter-box-ui repo
cp /var/www/html/assets/css/butter-dir-listing.css /var/www/html/butter-dir-listing.css
cp /var/www/html/assets/js/butter-dir-listing.js /var/www/html/butter-dir-listing.js

# Install the config file for the USB viewer.
cp $butter_src_dir/configs/50-usb-butter.conf /etc/lighttpd/conf-available/
# Don't link to -enabled yet, we let udev-triggered scripts do that conditionally.

# Create a udev rule that mounts the /butter directory of a newly inserted USB drive.
cp $butter_src_dir/configs/99-usb-butter.rules /etc/udev/rules.d/

# Script udev can run to configure the webserver.
cp $butter_src_dir/scripts/on-usb-drive-mounted.sh /usr/bin/
chmod +x /usr/bin/on-usb-drive-mounted.sh

udevadm control --reload-rules
systemctl daemon-reload