# Caja Mantequilla en Raspberry Pi

Esta guía está dirigida a los "Operadores  Mantequilla", los administradores
locales de Cajas Mantequilla desplegadas en comunidades de todo el
mundo. También es para desarrolladores de Cajas Mantequilla. Parte de esto
es bastante técnico, así que utiliza las partes de esta guía con las que te
sientas cómodo. Te recomendamos que te comuniques con nosotros y con la
comunidad para obtener ayuda con piezas que superen tu nivel de habilidad o
comodidad.


**Mesa de Contenidos**

[[_TOC_]]

## ¿Qué es una Caja Mantequilla?

Una [Caja Mantequilla](https://likebutter.app/box) es una Raspberry Pi u
otra computadora que ejecuta un conjunto de servicios que hacen que la vida
sin Internet sea un poco más fluida. Una Caja de Mantequilla contiene:

* Una página de inicio fácil de usar
* La tienda de aplicaciones Mantequilla & la aplicación Mantequilla para
  acceder a ella
* Contenido Cifrado [Matrix](https://matrix.org/) chat accesible via [Keanu
  Weblite](https://gitlab.com/keanuapp/keanuapp-weblite)

## Creación de una Caja Mantequilla a partir de una imagen (recomendado; consulta las advertencias)

Puedes usar [Raspberry Pi Imager](https://www.raspberrypi.com/software/)
para escribir una imagen de ButterBox en tu propia tarjeta SD y colocarla en
tu RPi. Cuando lo inicies, tendrás una Caja Mantequilla en pleno
funcionamiento.

Las imágenes de la Caja Mantequilla están actualmente disponibles en [esta
carpeta de Dropbox]
(https://www.dropbox.com/sh/y6ei9l7ixrrw04q/AAAgRsH-Ird619ioxEmeibNta?dl=0).
Las ligas a versiones en IPFS estan disponibles en  [the Butter Box
website](https://likebutter.app/box).   Los archivos se denominan
`bbb-[idioma principal]-[commit used to provision].img`.

### Advertencias

Esta imagen viene precargada con secretos públicos. La mayoría de estos se
pueden cambiar siguiendo las instrucciones de este documento.

No utilices el método de imagen para instalar si estás en un entorno donde
alguien quiera espiar lo que estás haciendo. Úsalo si vas a probar la Caja
Mantequilla o planeas usarla para comunicaciones no confidenciales. Resolver
[este problema](https://gitlab.com/likebutter/butterbox-rpi/-/issues/29)
hará que las instalaciones basadas en imágenes sean apropiadas para casos de
uso sensibles.

## Crear una Caja Mantequilla desde cero

Este es un enfoque más técnico que requiere más tiempo y una buena conexión
a Internet. Sin embargo, te permite crear la última versión de la Caja
Mantequilla y personalizar su instalación editando el script
`buttermeup.sh`.

### Requerimientos

* Raspberry Pi
	* Por lo general, construimos sobre una Raspberry Pi 4, pero hemos probado
	  imágenes en Raspberry Pi Zero W 2.
	* Ten en cuenta que puedes tener problemas para usar un Zero W (2) para
	  construir el servidor de matriz. También será mucho, mucho más lento.
* Una computadora para crear imágenes de una tarjeta Raspberry Pi Micro
  SD. Dependiendo de los puertos que tenga su computadora, es posible que
  necesites un adaptador para conectarte a la tarjeta MicroSD.
* Conexión a Internet para la raspberry pi a través de ethernet (preferido)
  o conexión WiFi. Esto solo es necesario para la configuración.

### Usando el script `buttermeup.sh`

1. Crea una imagen del RPi.
	* Con Raspberry Pi Imager, selecciona la imagen Raspberry Pi OS Lite (64
	  bits).
		* Si deseas utilizar hardware RPi más antiguo, como Pi Zero W 1, es
		  posible que necesites usar una imagen base de 32 bits y deshabilitar o
		  solucionar los componentes de la instalación que requieren una
		  arquitectura de 64 bits (por ejemplo, Flutter- basado en
		  fdroid-webdash).
	* Usando la configuración avanzada (Ctrl+Shift+x):
		* Establezca el nombre de host en "butterbox"
		* Habilita SSH, establezca una contraseña adecuada para tu escenario; las
		  unidades de prueba usan "mantequilla".
		* Configura el país wifi a tu país. Si no deseas que el RPi se conecte
		  automáticamente a tu red inalámbrica (es decir, planeas usar Ethernet),
		  un SSID/contraseña ficticio servirá (por ejemplo, SSID: Foo; Contraseña:
		  bar).
	* Graba la imagen en la tarjeta SD.
2. Actualiza el sistema operativo y reinicia.
	* `sudo apt-get update && sudo apt-get full-upgrade -y && sudo apt-get
	  install git -y && sudo reboot`
3. Copia este repositorio en el RPi.
	* Si este repositorio ya está en tu computadora, has `cd` en él y luego
	  ejecuta `rsync -r ./ pi@butterbox.local:/tmp/butter-setup` para copiarlo
	  al RPi.
	* O, si este repositorio está disponible de forma remota, SSH en el RPi y
	  clona el repositorio `git clone
	  https://gitlab.com/guardianproject/wind-offline-fdroid-repo-on-rpi.git
	  /tmp/butter-setup` (esto puede requerir credenciales).
4. Ejecuta el script de configuración en el RPi a través de SSH.
	* Ejecuta `sudo bash /tmp/butter-setup/scripts/buttermeup.sh`para
	  conseguirlo
	* Si la conexión inestable con el RPi, puedes usar `tmux` ejecutando
	  `/tmp/butter-setup/scripts/tmuxify.sh` en lugar de `buttermeup.sh`. Esto
	  también muestra htop y tail de los registros de instalación, que son
	  útiles para la depuración.
	* Puedes especificar un idioma y SSID/dominio que te gustaría usar usando
	  banderas:
		* `-l` establece el idioma. `en` es el valor predeterminado, pero `es`
		  también es compatible. Esto afecta el portal cautivo predeterminado y la
		  página de inicio a la que se sirve el usuario. Cuando se elige `es`, el
		  SSID y el nombre de host predeterminado serán comolamantequilla[.lan].
	* Puedes especificar el indicador `-c` para que el script copie
	  automáticamente una imagen de la instalación nueva en el dispositivo
	  `/dev/sda`. El archivo se llamará bbb.img (copia de seguridad de
	  butterbox).
		* Este dispositivo debe ser más grande que el disco en el que ha
		  aprovisionado el RPI, ya que primero se copiará por completo y luego se
		  reducirá para eliminar el espacio vacío.
		* Si deseas utilizar .img en una Mac, puedes formatear una unidad USB como
		  ExFAT, que se podrá escribir en el RPi y se podrá leer en tu Mac.
		* Se tarda aproximadamente una hora en copiar (luego reducir) una imagen
		  de 32 GB.
5. La instalación puede demorar aproximadamente una hora, según la red y el
   rendimiento de RPi. El RPi se reiniciará automáticamente y debería
   transmitir la red "butterbox" o "comolamantequilla" cuando vuelva a
   funcionar. Si eso falla, desconecta el RPi, espera 10 segundos, y
   conectalo nuevamente.

## Moderar, configurar y personalizar tu Butter Box

### ¿Cómo instalar un Butter Box?

Aside from [setting appropriate passwords](#setting-appropriate-passwords),
remember:

* If the box isn't physically secure, people could tamper with it.  They
  could replace the softare running on it, or insert a USB stick with
  viruses or other exploits.  Consider whether yours should be locked up.
* The public room is public. if you need E2E encryption, make sure create a
  private room, share that room's invite with only trusted parties, and
  change "Join Permissions" to "Only People Added" once they've arrived.
* If you do enable IPFS, remember that while the UI exposes just simple
  upload functions, the full API is exposed to any Butter Box user.  This
  might be appropriate for a small, trusted group, but probably not for the
  general public.
* Butter Boxes run on MicroSD cards.  These frequently fail, causing all the
  data on them to be destroyed.  MicroSD cards can also be stolen.
  Attackers could also attempt to fill the drive, leaving you with no more
  available space.  Backup irreplaceable data when possible.
* [Butter Boxes do not run
  TLS](https://gitlab.com/likebutter/butterbox-rpi/-/issues/11) and their
  wifi network is open by default.  This means URLs you visit on the box,
  including the names of rooms you connect to may be exposed to anyone
  monitoring traffic (see note about changing "Join Permissions" above).

### Establecer contraseñas apropiadas

Las imágenes de la Caja Mantequilla que distribuimos tienen un conjunto
estándar de contraseñas. Debido a que se comparten públicamente, no son
seguras. Debes cambiar las contraseñas para evitar el acceso no autorizado a
tu Caja Mantequilla.

* SSH
	* De forma predeterminada, el usuario `pi` tiene la contraseña
	  `butterbox-admin`.
	* Cambie esta contraseña ingresando al pi y ejecutando `passwd`.
	* Si prefieres utilizar una clave SSH, asegúrate de deshabilitar el acceso
	  con contraseña una vez que habilites el acceso basado en claves.
* RaspAP
	* El punto de acceso tiene una interfaz administrativa que se puede
	  utilizar para cambiar su configuración.
	* Valores predeterminados: usuario: `admin`, contraseña: `secret`
	  (irónicamente, esto no es secreto).
	* Cambia esto iniciando sesión en http://butterbox.lan/admin (o
	  http://comolamantequilla.lan/admin para un cuadro en español) y usando la
	  interfaz de usuario web.
* Chat
	* La sala pública fue creada por un usuario administrativo llamado
	  `butterbox-admin`. La contraseña de este usuario también es
	  `butterbox-admin`.
	* Cambia esta contraseña iniciando sesión en la Caja Mantequilla, yendo a
	  la sala de chat pública, luego visitando tu perfil de usuario y
	  actualizando la contraseña. Es posible que también desees cambiar el
	  _nombre_ de `butterbox-admin` para que otros usuarios lo reconozcan.

Recuerda que cualquier persona con acceso físico a la Caja Mantequilla
podría manipularla, incluido el restablecimiento de las contraseñas SSH. Si
eso te preocupa, manten la caja bajo llave en un lugar seguro.

### Moderar la sala de chat pública

Cuando inicias sesión en la sala de chat como usuario administrativo
`butterbox-admin`, puedes eliminar mensajes inapropiados, expulsar a los
usuarios de la sala y otorgar privilegios de administrador a otros usuarios.

### Configurar un nuevo SSID o agregar una contraseña a la red WiFi

Dependiendo del idioma predeterminado de tu Butter Box, la red se llamará
`butterbox` o `comolamantequilla`. Puedes cambiar esto usando la interfaz de
administración de RaspAP disponible en http://butterbox.lan/admin (o
http://comolamantequilla.lan/admin para un cuadro en español). El nombre de
usuario predeterminado es "admin" y la contraseña predeterminada es
"secreta".

Usando la misma interfaz, puedes agregar una contraseña a la red para que
solo aquellos a quienes les dés la contraseña puedan unirse. Considera si
esto es adecuado para tu caso de uso.

Si cambias el SSID o la contraseña, los códigos QR que se encuentran en esta
documentación y en los kits de la Caja Mantequilla que distribuimos ya no
funcionarán. Deberás crear un nuevo código QR o pedirle a alguien que
escriba la contraseña para poder unirse a tu red.

### Agregar contenido personalizado al cuadro

The Butter Box will automatically display the contents of a USB drive when
plugged in.  You can use this to share your own files or you can use a USB
drive to host one or more premade "Content Packs".

If you don't want to use a USB drive, you (or any user) can also simply post
files to the chat room to share with other users.

#### Premade Content Packs and Content Pack Recipes:

* Maps
	* You can host a completely offline map with detail down to the building
	  level!.  The maps themselves are based off of OpenStreetMap and the
	  technology to host them off of
	  [PMTiles](https://github.com/protomaps/PMTiles) and
	  [MapLibre](https://maplibre.org/).
	* Here's [a sample Content
	  Pack](https://www.dropbox.com/s/fa12ej1n18r13hy/maps.zip?dl=0) (use the
	  download link to download this `.zip` file) which contains just the map
	  of Madison, WI.  We can generate you a new mapfile for any geography (or
	  you can do it yourself follwing instructions
	  [here](https://docs.protomaps.com/guide/getting-started)).
* Mantequilla Digital Security Repo
	* "This is a collection of Spanish language videos (the "Watch" folder) and
	  guides ("Learn") that teach digital security for various situations."
	* You can download [the zip
	  file](https://drive.google.com/drive/u/0/folders/1gqD0-liFdXrDO4ettGDtCB_emIw2Syku)
	  and unzip it onto a USB drive.
* OpenCourseWare
	* MIT OpenCourseWare offers a static site of their catalog.  You can try it
	  out [online
	  here](https://ocw-content-offline-live-production.s3.amazonaws.com/index.html).
	* It weighs in at over 1TB and lives on Amazon S3.  You can sync it to a
	  USB drive using `aws s3 sync s3://ocw-content-offline-live-production
	  /path/to/your/usb-drive`.
* News
	* You can use a ["Sneakernet"](https://en.wikipedia.org/wiki/Sneakernet) to
	  carry news to the Butter Box and distribute it to other users.
	* Some news sites are already published as a static site and can be loaded
	  directly onto a USB drive.  For example, the [Guardian Project
	  podcast](https://guardianproject.info/podcast/) website is built as a
	  static site which you can download from gitlab
	  ([example](https://gitlab.com/guardianproject/public-content/engarde-podcast/-/jobs/4651713918/artifacts/download?file_type=archive))
	  and place on a USB drive.
	* You can also use [AnyNews](https://gitlab.com/guardianproject/anynews) to
	  generate a static site while you have an Internet connection, then load
	  that onto the box.
	* Lastly, for websites that aren't built as a static source, you can save
	  them as a static site using tools like
	  [HTTrack](https://www.httrack.com/).

To use a content pack, simply put the files on a USB drive (remember to
"unzip" any `.zip` files first).  Then, insert that drive into the Butter
Box.

#### How to Broadcast content from a USB Drive

If you're using a Pi Zero W 2 or another device, you may need to use an
adapter [like this](https://www.amazon.com/gp/product/B00LN3LQKQ/) to plug
in a USB drive.

When the drive is plugged in, a new section will appear on the homepage
providing the option to view the files an administrator has made available.
Users will be able to browse through subfolders, selecting files by name.
Files which can be natively displayed in a browser, like images, will
display with a click, while other files, like .apk app files, will be
downloaded to the device.

Unplugging the USB drive will cause the section on the homepage to
disappear.

#### Distributing your own files

You can put anything you want on your USB drive including a mix of content
packs and personal content.

If you want to be fancy, you can include an `index.html` in the root folder
or any subfolders and it'll be served instead of a directory listing.  This
provides the opportunity for substantial customization.

## Solución de problemas

1. Si es la primera vez que conectas una Caja Mantequilla nueva, pueden
   pasar varios minutos antes de que veas por primera vez la red
   WiFi. Después de este primer arranque, debería arrancar mucho más rápido.
2. Desconectar la Caja Mantequilla, esperar 10 segundos y luego volver a
   conectarla resolverá la mayoría de los problemas.
3. Si te sientes cómodo con Linux, has `ssh` en la Caja Mantequilla y
   verifica el estado de los servicios problemáticos. `systemctl` y
   `journalctl` son buenas herramientas y los principales servicios son:
	* `lighttpd`: el servidor web que aloja la página de inicio y la interfaz
	  de administración de RaspAP
	* `hostapd`: transmite el punto de acceso local.
	* `ipfs`: el nodo local IPFS
	* `dendrite`: el servidor del chat.
4. Siempre puedes cambiar la imagen de tu Caja Mantequilla siguiendo las
   instrucciones anteriores. Esto borrará todos los datos (por ejemplo,
   chat, cargas de IPFS) y cuentas de usuario que ya estén en la Caja
   Mantequilla, así que hágalo solo como último recurso o si ninguno de sus
   usuarios necesita datos del pasado. Si no puedes hacer esto, comunícate
   con el equipo, quien quizás pueda enviarte una postal que contenga una
   nueva tarjeta MicroSD.

## IPFS integrado

Note: This feature is turned off by default.

La Caja Mantequilla viene con un nodo [IPFS](https://ipfs.tech/) integrado y
una interfaz de usuario web para cargar archivos a IPFS. Cuando un usuario
carga un archivo, el nodo local fijará el archivo y devolverá el hash del
archivo recién cargado al usuario.

### Descargar y ver archivos cargados

Utiliza la CLI de IPFS para enumerar los archivos anclados por la Caja
Mantequilla. SSH en el cuadro y use `ipfs pin ls --type=recursive` para ver
la lista de CID. Alternativamente, puedes usar `$ curl -X POST
"http://butterbox.lan:5001/api/v0/pin/ls?type=recursive"` desde tu máquina
local conectada a una butterbox para obtener una lista de pins con formato
JSON.

Para descargar archivos, puedes utilizar su cliente IPFS local. Al ejecutar `ipfs get <CID>` se descargará el archivo a tu computadora, aunque sin extensión. Si conoces el nombre de archivo y la extensión que se deben aplicar a ese CID, ¡excelente! Si no, tu navegador a menudo reconocerá el tipo de archivo. Arrastra el archivo a tu navegador para obtener una vista previa y/o guardarlo con una extensión adecuada. Alternativamente, puedes usar `➜ curl -X POST "http://butterbox.lan:5001/api/v0/get?arg=<CID>" --output <filename.zip>` para obtener un ZIP que contenga los archivos CID.

### Hacer que los archivos anclados estén disponibles en la red principal de IPFS

Si (o cuando) Butter Box esté conectado a Internet, el archivo estará
disponible en la red IPFS principal. Si deseas que estos CID permanezcan
accesibles después de que Butter Box se desconecte y se regrese a su hogar
fuera de línea, deberás solicitar un servicio de fijación para fijar esos
CID.
