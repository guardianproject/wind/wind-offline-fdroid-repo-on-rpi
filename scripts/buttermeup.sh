#!/bin/bash

# Turn a freshly imaged RPi into a butterbox.  Execute with sudo.
# copy this whole directory to the newly imaged RPi with
# rsync -r ./ pi@butterbox.local:/tmp/butter-setup
# Then ssh into the box and run this script
# sudo -E bash /tmp/butter-setup/scripts/buttermeup.sh
# If your terminal connection to the pi is flakey, try:
# sudo apt-get install screen -y && screen sudo -E bash /tmp/butter-setup/scripts/buttermeup.sh

# If any command in this script fails, exit.
set -e

language="en"
copy_image_to_sda=false

while getopts ":l:c" flag
do 
	case "${flag}" in
		l) language=${OPTARG};;
		c) copy_image_to_sda=true;;
	esac
done

case $language in
	en) name="butterbox";;
	es) name="comolamantequilla";;
	# These SSIDs are direct from butter-box-ui's site_name field ("butter" in en)
	zh_Hans) name="黄油";;
esac

echo "Setting up butterbox with";
echo "    SSID:             $name";
echo "    Default language: $language";
echo "    Copying to sda?   $copy_image_to_sda";
echo "";
export butter_language=$language
export butter_name=$name

mkdir -p /var/log/butter
chmod a+w /var/log/butter
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
echo "Script Dir: $SCRIPT_DIR" >> /var/log/butter/install.txt
butter_src_dir=$(dirname $SCRIPT_DIR)
echo "Provision Dir: $butter_src_dir" >> /var/log/butter/install.txt
export butter_src_dir

# Naively retry each step up to 3 times.
# Steps are not all idempotent, so as we run into snags with steps failing,
# we'll need to update them to be idempotent.
retry_command() {
    local max_retries=3
    local count=0
    local command=$@

    while [ $count -lt $max_retries ]; do
		# sleep 5 times the count seconds (that's 0 for first time)
		sleep $((count*5))
        $command && break  # Execute the command and break if it's successful
        count=$((count+1))
        echo "Attempt $count of $max_retries failed."
    done

    if [ $count -eq $max_retries ]; then
        echo "Command failed after $max_retries attempts: $command"
		exit 1
    fi
}

apt-get update

echo "Installing RaspAP"
retry_command bash $SCRIPT_DIR/install-raspap.sh >> /var/log/butter/install.txt 2>&1

echo "Installing Connection Counter"
retry_command bash $SCRIPT_DIR/install-counter.sh >> /var/log/butter/install.txt 2>&1

echo "Installing Butter Site"
retry_command bash $SCRIPT_DIR/install-butter-site.sh >> /var/log/butter/install.txt 2>&1

echo "Installing USB Viewer"
retry_command bash $SCRIPT_DIR/install-usb-viewer.sh >> /var/log/butter/install.txt 2>&1

echo "Installing Matrix Chat"
retry_command bash $SCRIPT_DIR/install-chat.sh >> /var/log/butter/install.txt 2>&1

echo "Installing AP-optimized wifi firmware"
retry_command bash $SCRIPT_DIR/install-ap-optimized-firmware.sh >> /var/log/butter/install.txt 2>&1

echo "Removing wifi creds from wpa_supplicant"
retry_command bash $SCRIPT_DIR/remove-wifi-creds.sh >> /var/log/butter/install.txt 2>&1

if [ "$copy_image_to_sda" = true ] ; then
	echo "Imaging this RPi Install"
	retry_command bash $SCRIPT_DIR/make-img.sh >> /var/log/butter/install.txt 2>&1
fi

echo "Butter Box successfully provisioned." >> /var/log/butter/install.txt
reboot