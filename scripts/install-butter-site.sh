set -e


# if /tmp/butter-site exists, delete it
if [ -d "/tmp/butter-site" ]; then
  rm -rf /tmp/butter-site
fi

if [ -f "/tmp/site.zip" ]; then
  rm /tmp/site.zip
fi
curl https://likebutter.gitlab.io/butter-box-ui/site-$butter_language.zip -o /tmp/site.zip
if [ -d "/tmp/butter-site" ]; then
  rm -rf /tmp/butter-site
fi
unzip /tmp/site.zip -d /tmp/butter-site
cp -r /tmp/butter-site/* /var/www/html/