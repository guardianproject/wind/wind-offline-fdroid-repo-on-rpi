set -e

# This loads a firmware that takes up less of the SRAM used by the wifi chip
# which allows more simultaneous connections (~19 instead of 4-8).
# Using this method rather than changing the symlink directly, should survive
# OS/package updates.  You can check which firmware is in use by running
# sudo update-alternatives --config cyfmac43455-sdio.bin.  The one we want
# to see is "minimal".
# This firmware is non-free.
# See: https://github.com/RPi-Distro/firmware-nonfree/tree/bullseye/debian/config/brcm80211/cypress
update-alternatives --set cyfmac43455-sdio.bin /lib/firmware/cypress/cyfmac43455-sdio-minimal.bin