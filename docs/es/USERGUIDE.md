# Guia de Usuario de la Caja de Mantequilla

## ¿Qué es una Caja de Mantequilla?

Cuando no puedes acceder a Internet, la Caja Mantequilla hace que la vida sin conexión sea un poco más sencilla.  

Una Caja Mantequilla es un dispositivo al que puedes acceder mediante la
conexión wifi de tu smartphone o tu ordenador. Cuando estés conectado,
puedes. instalar aplicaciones de Android como Open Street Maps, un manual de
supervivencia o incluso juegos para entretener a sus hijos (¡o a ti
mismo!). También puedes utilizar la Caja Mantequilla para chatear de forma
pública o privada.

## Configuración de una Caja de Mantequilla

Simplemente conéctala usando el adaptador de corriente incluido o una fuente
de alimentación micro USB adecuada. En unos segundos, verás que está
transmitiendo la red wifi "butterbox".

![Conecta la Caja Mantequilla](images/plugged-in-to-outlet.jpg){width=40%}

![Conecta la Caja Mantequilla a un panel
solar](images/plugged-in-to-solar.jpg){width=40%}

La caja Mantequilla funciona sin conexión a Internet y viene precargada con
todo lo que necesitas. La primera vez que la conectes, es posible que tarde
varios minutos en iniciarse. Después de eso, comenzará más rápido.

## Conectarse a la Caja Mantequilla

Con un dispositivo Android, conéctate a la red wifi de Butterbox. Para
asegurarte de que tu dispositivo busque información de la Caja Mantequilla
en lugar de Internet, apaga la conexión celular de tu teléfono.

![Conectarse a la red de Wifi de
Mantequilla](images/en/qr/butterbox.lan.png){width=40%}

A continuación, visite `http://butterbox.lan`. Puedes visitar esa dirección
escaneando este código QR:

![Conectarse a la red de Wifi de
Mantequilla](images/en/qr/butterbox.lan.png){width=40%}

### Instalación de aplicaciones de Android

Una vez que haya conectado y cargado la página de inicio de la Caja
Mantequilla, has clic en "Descargar aplicación de Android" y luego has clic
en "Descargar aplicación de Android" nuevamente desde el modal. Esto
descargará un archivo llamado "butter.apk" a tu dispositivo.

![Descargar Mantequilla.](images/en/download.jpg){width=40%}

Abre "mantequilla.apk". Es posible que debas permitir la instalación desde
fuentes desconocidas. Esto instalará la aplicación Mantequilla, que ahora
puedes abrir.

![La aplicación de mantequilla en la página de
inicio](images/en/app.jpg){width=40%}

Después de que la aplicación Mantequilla cargue el repositorio, verás una
lista de las aplicaciones disponibles. Has clic en cualquier aplicación para
leer más y luego en "Instalar" para instalar la aplicación en tu
dispositivo.

![Lista de aplicaciones de la tienda de
Mantequilla](images/en/butter-app-store.jpg){width=40%}

### Chatear con otros usuarios de la Caja Mantequilla

Una vez que te hayas conectado y cargado la página de inicio de la Caja
Mantequilla, puedes elegir unirse a una sala de chat pública o iniciar un
chat privado usando los botones en la página de inicio http://butterbox.lan.

Al igual que un tablero de anuncios, puedes ver todos los mensajes que otros
han dejado en el chat público y cualquier mensaje que dejes en el chat
público estará disponible para otras personas que se conecten más tarde.

Si deseas tener una conversación privada, puedes crear tu propia sala de chat. Cuando creas una sala privada, solo aquellos a quienes invitas pueden ingresar y todos los mensajes que envías se cifrarán para que solo tú y tus invitados puedan verlos. Ni siquiera los creadores o administradores de la Caja Mantequilla pueden ver tus mensajes privados.  

Para invitar a otras personas a la sala de chat que creaste, has clic en el
ícono en la parte superior izquierda, luego has clic en el código QR pequeño
para agrandar el código QR para que tus amigos puedan escanearlo. Tus amigos
se unen a la sala conectándose al wifi de la Caja Mantequilla y luego
escaneando el código QR.

## Solución de problemas

Si te has conectado al wifi, pero no puedes cargar la página de inicio,
asegúrate de que tus datos móviles estén desactivados.

Si la Caja Mantequilla no funciona correctamente, el paso más sencillo es
reiniciarla desconectándola, esperando 10 segundos y volviéndolo a
conectarla.

Si reiniciar la Caja Mantequilla no soluciona el problema, pide ayuda a tu
administrador.
